Dado("que estou na tela de cota diária") do
  expect(page).to have_current_path("http://192.168.3.54/pointercombustivel360/?UserId=26349&AccountId=21777&TokenId=18312120210025533#/cota-diaria", url: true)
  sleep(5)
end

Quando("informo uma chapa {string}") do |chapa|
  @chapa = chapa
  find("#plate").set @placa
end

Quando("informo uma placa {string}") do |placa|
  @placa = placa
  find("#licensePlate").set @placa
end

Quando("informo um motorista {string}") do |motorista|
  @motorista = motorista
  find("#react-select-2-input").set @motorista
  find("#react-select-2-input").send_keys :tab
  sleep(8)
end

Quando("informo um status da cota {string}") do |status_cota|
  @status_cota = status_cota
  find("#react-select-3-input").set @motorista
  find("#react-select-3-input").send_keys :tab
  sleep(8)
end

Quando("informo o período") do
  find(:css, ("button[class='react-daterange-picker__calendar-button react-daterange-picker__button']")).click
  find(:css, ("button[class='react-calendar__navigation__arrow react-calendar__navigation__prev-button']")).click
  find(:css, ("abbr[aria-label='25 de fevereiro de 2020']")).click
  find(:css, ("abbr[aria-label='25 de março de 2020']")).click
end

Quando("clico em buscar") do
  click_button "Pesquisar"
  sleep(10)
end

Então("o sistema deve exibir o resultado da pesquisa") do
  # page.has_css? ("div[role='grid']")
  # @resultado_placa = find(:css, ("div[role='gridcell']"))
  # expect(@resultado_placa.text).to eql placa
end
