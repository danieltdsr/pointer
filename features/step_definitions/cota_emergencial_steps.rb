Dado("que estou na tela de cota emergencial") do
  find("div[class='dropdown nav-item']", match: :first).click
  find("a[href*='#/cota-emergencial']", match: :first).click
  expect(page).to have_current_path("http://192.168.3.54/pointercombustivel360/?UserId=26349&AccountId=21777&TokenId=18312120210025533#/cota-emergencial", url: true)
end

Quando("informo um {string}") do |motorista|
  @motorista = motorista
  find("div[class=' css-1wa3eu0-placeholder']", match: :first).set @motorista
  sleep(2)
  find("div[class=' css-1wa3eu0-placeholder']", match: :first).send_keys :tab
end

Quando("informo o motivo {string}") do |motivo|
  @motivo = motivo
  find("div[class=' css-1uccc91-singleValue']", match: :first).set @motivo
  find("div[class=' css-1uccc91-singleValue']", match: :first).send_keys :tab
end

Quando("informo o valor {string}") do |string|
  pending # Write code here that turns the phrase above into concrete actions
end

Quando("informo uma justificativa {string}") do |string|
  pending # Write code here that turns the phrase above into concrete actions
end

Quando("clico em enviar") do
  pending # Write code here that turns the phrase above into concrete actions
end

Então("o sistema deve exibir uma mensagem de sucesso") do
  pending # Write code here that turns the phrase above into concrete actions
end

Então("exibir tela de aprovações pendentes") do
  pending # Write code here that turns the phrase above into concrete actions
end
