Dado("que estou com o navegador aberto") do
end

Quando("informo a URL do portal com parâmetros válidos") do
  visit "http://192.168.3.54/pointercombustivel360/?UserId=26349&AccountId=21777&TokenId=18312120210025533#/cota-diaria"
end

Então("o página home deve ser acessada") do
  expect(page).to have_current_path("http://192.168.3.54/pointercombustivel360/?UserId=26349&AccountId=21777&TokenId=18312120210025533#/cota-diaria", url: true)
  @titulo_tela = find(".title-page", match: :first)
  expect(@titulo_tela.text).to eql "Cotas diárias enviadas"
end

"http://192.168.3.54/pointercombustivel360/?UserId=26349&AccountId=21777&TokenId=18312120210025533#/unauthorized"
"http://192.168.3.54/pointercombustivel360/?UserId=26349&AccountId=21777&TokenId=18312120210025533#/cota-diaria"
