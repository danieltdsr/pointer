#language:pt

Funcionalidade: Pesquisa cota diária

Essa Funcionalidade deve validar a pesquisa de uma cota diária e exibição do resultado em tela

#FALTA VALIDAR RESULTADO DA PESQUISA
@login
@pesquisa_chapa
Cenário: Pesquisa por chapa válida

    Dado que estou na tela de cota diária
    Quando informo uma chapa "45485"
    E informo o período
    E clico em buscar
    Então o sistema deve exibir o resultado da pesquisa

#FALTA VALIDAR RESULTADO DA PESQUISA
@login
@pesquisa_placa
Cenário: Pesquisa de cota por placa válida

    Dado que estou na tela de cota diária
    Quando informo uma placa "OBI-6600"
    E informo o período
    E clico em buscar
    Então o sistema deve exibir o resultado da pesquisa

#FALTA VALIDAR RESULTADO DA PESQUISA E INSERÇÃO DO MOTORISTA
@login
@pesquisa_motorista
Cenário: Pesquisa de cota por motorista válido

    Dado que estou na tela de cota diária
    Quando informo um motorista "MARCO DE SOUZA LIMA"
    E informo o período
    E clico em buscar
    Então o sistema deve exibir o resultado da pesquisa

#FALTA VALIDAR RESULTADO DA PESQUISA E INSERÇÃO DO STATUS
@login
@pesquisa_status_cota
Cenário: Pesquisa de cota por status da cota válido

    Dado que estou na tela de cota diária
    Quando informo um status da cota "Pendente de Aprovação"
    E informo o período
    E clico em buscar
    Então o sistema deve exibir o resultado da pesquisa