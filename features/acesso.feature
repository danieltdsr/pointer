#language:pt

Funcionalidade: Acesso

Essa Funcionalidade deve validar o login na plataforma combustível 360 seguindo as regras de negócio
de "UserId/AccountId/TokenId" quando um usuário só pode logar na plataforma quando informar essas 
informações válidas

@acesso
Cenário: Acesso com informações válidas 

    Dado que estou com o navegador aberto 
    Quando informo a URL do portal com parâmetros válidos
    Então o página home deve ser acessada

