#language:pt

Funcionalidade: Lançamento de cota emergencial

Essa Funcionalidade deve validar o lançamento de uma cota emergencial

@login
Cenário: Lançamento de cota emergencial por divergência de desempenho

    Dado que estou na tela de cota emergencial
    Quando informo um "EFERSON PEREIRA DA SILVA"
    E informo o motivo "Divergência de desempenho"
    E informo o valor "0,01"
    E informo uma justificativa "teste"
    E clico em enviar
    Então o sistema deve exibir uma mensagem de sucesso
    E exibir tela de aprovações pendentes